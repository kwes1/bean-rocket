//probably dont mess with these variables 
let x = 50;
let y = 300;
let vecx = 0
let vecy = 0
let yD = 21
let hgSP = 0
let screenX = 405
let ranYD
let plume = false
let hgt = 25
const q = 0.00000003
let moonRan
//you can mess with these (if im being honest my coding skills in 2021 were so bad i dont know what half of these things control)
let maxfuel = 0               //  fuel you can store (it says 0 cause its the added max fuel when you upgrade, EXAMPLE: change maxfuel to 100, you have 200 actual max fuel)
let fuel = 100               //   current fuel
let speedlim = 3            //    max velocity you can go (3 default)
let ore = 0                //     moon ore you mine from moon
let friction = 0.025      //      friction from the air (does not affect the rocket inspace probably) (0.025 default)
let gravity = 0.05       //       gravity.. (0.05 default)

function setup() {
  createCanvas(400, 400);
  background(255);
  moonRan = random(-200,100);
}

function draw() {
  noStroke()
  background(20, 185, 220);
  if (plume == true) {
    ranYD = random(1,4)
  }
  
  vecy = vecy + gravity 
  x = x + vecx
  y = y + vecy
  
  yD = 9

  //friction
  if (vecx >= q) {
    vecx = vecx - friction
  }

  if (vecx <= -q) {
    vecx = vecx + friction
  }

  if (vecy >= q) {
    vecy = vecy - friction 
  }

  if (vecy <= -q) {
    vecy = vecy + friction
  }

  // speed limit
  if (vecx >= speedlim) {
    vecx = 2.8
  }

  if (vecx <= -speedlim) {
    vecx = -2.8
  }

  if (vecy >= speedlim + 2) {
    vecy = 4.8
  }

  if (vecy <= -speedlim - 2) {
    vecy = -4.8
  }

  //if at edge
  if (x >= 390) {
    x = 389.9
    vecx = 0
  }

  if (x <= -5) {
    x = -4
    vecx = 0
  }

  if (y <= -5) {
    y = -4.9
    vecy = 0
  }

  if (y >= 300 - hgSP){
    y = 299.99 - hgSP
    vecy = vecy - 0.2
  }

  //in or out space
  if (y >= 121){
    friction = 0.025
    gravity = 0.05
    speedlim = 3
  }

  if (y <= 120){
    friction = 0.0008
    gravity = 0.0008
    speedlim = 4
  }
  
  //moon collision
  if (x >= 200+moonRan && x <= 250+moonRan &&
    y >= 48 - hgSP && y <= 49.9) {
    y = 48 - hgSP
    vecy = 0.0001
  }

  //boom
  if (y >= 299 && vecy >= 1.8){
    y = 300
    x = 50
    vecx = 0
    vecy = 0
     if(ore >= 0.5){
      ore = ore - 1
    }
  }

  //refuel station
  if(y >= 295 - hgSP && x >= 350) {
    if (fuel <= 100 + maxfuel){
      fuel = fuel + .12
    }
    if(ore >= 10) {
      screenX = 30
      vecx = 0
      vecy = 0
      if (keyIsDown(90)) {
        ore = ore - 10
        maxfuel = maxfuel + 25
        hgt = hgt + 5
        hgSP = hgSP + 5
      }
  } else {
    screenX = 405
  }
  }

  //keys & fuel
  if(fuel >= 0){
    if (keyIsDown(LEFT_ARROW)) {
      vecx -= 0.07
      fuel = fuel - 0.019
    }

    if (keyIsDown(RIGHT_ARROW)) {
      vecx += 0.07
      fuel = fuel - 0.019
    }

    if (keyIsDown(UP_ARROW)) {
      vecy -= 0.10;
      yD = 20 + hgSP
      fuel = fuel - 0.08
      plume = true
    }

    if (keyIsDown(DOWN_ARROW)) {
      vecy += 0.10;
      yD = -10 - hgSP / 50
      fuel = fuel - 0.08
      plume = true
    }
  }

  //mine
  if (keyIsDown(69) && y == 48 - hgSP && ore <= 75) {
    ore = ore + 0.035
    fuel = fuel - 0.04
  }


  //shop screen
  fill(210)
  rect(screenX,145,290,120,10)
  fill(50);
  textFont("Comic Sans MS");
  textSize(11)
  text("max fuel upgrade -- press z to buy --cost: 10 moon ore ",screenX + 5, 175);

  //grass and space
  fill(60,190,110)
  rect(0,320,400,300)
  fill(10,50,80)
  rect(0,-5,400,125)

  //planet

  fill(150)
  rect(200+moonRan, 70, 60, 15, 10)
  rect(207.5+moonRan, 80, 45, 15, 10)
  rect(216+moonRan, 90, 20, 15, 10)
  
  //text
  let vecyD = round(vecy);
  let vecxD = round(vecx);
  let fulD = round(fuel, 1);
  let vey = round(y);
  let vex = round(x);
  let oreD = round(ore, 1);
  let maxD = maxfuel + 100
  
  fill(50);
  textSize(13)
  textFont("Comic Sans MS");
  text("Y velocity: " +vecyD, 120, 385);
  text("X velocity: " +vecxD, 120, 370);
  text("fuel: " +fulD, 20, 345);
  text("max fuel: " +maxD, 20, 360);
  text("Y: " +vey, 220, 385);
  text("X: " +vex, 220, 370);
  text("moon ore: " +oreD, 20, 385);
  
  // refuel
  fill(50);
  rect(350,315,50,15,7)

  //player
  rotate(0);
  fill(250, 105, 20)
  rect(x+ranYD,y+yD,10,15,12)
  rotate(0);
  fill(200)
  rect(x, y, 15, hgt, 10); 
}
